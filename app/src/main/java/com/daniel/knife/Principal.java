package com.daniel.knife;

import android.os.Bundle;

import com.daniel.knife.AndGraph.AGActivityGame;
import com.daniel.knife.cenas.*;

public class Principal extends AGActivityGame {

    Splash splash = null;
    Home home = null;
    Game game = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Inicializa o motor gráfico
        init(this, false);

        splash = new Splash(getGameManager());
        home = new Home(getGameManager());
        game = new Game(getGameManager());

        //Registra a cena no gerenciador
        getGameManager().addScene(splash);
        getGameManager().addScene(home);
        getGameManager().addScene(game);
    }
}
