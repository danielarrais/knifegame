package com.daniel.knife.cenas;

import com.daniel.knife.AndGraph.AGGameManager;
import com.daniel.knife.AndGraph.AGInputManager;
import com.daniel.knife.AndGraph.AGScene;
import com.daniel.knife.AndGraph.AGScreenManager;
import com.daniel.knife.AndGraph.AGSoundManager;
import com.daniel.knife.AndGraph.AGSprite;
import com.daniel.knife.AndGraph.AGTimer;
import com.daniel.knife.AndGraph.AGVector2D;
import com.daniel.knife.R;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Game extends AGScene {

    //Atributos da classe: Placar, imagem score e a variavel para armazenar o score
    AGSprite[] vrPlacar = null;

    AGTimer tempo = new AGTimer(1500);

    int score;
    AGSprite boxum = null;
    AGSprite boxdois = null;
    AGSprite boxtres = null;

    int somBomb;
    int somVitoria;


    AGSprite gameover = null;
    AGSprite restart = null;
    AGSprite exit = null;

    boolean perdeu;
    boolean ganhou;
    boolean gover;

    SecureRandom random = new SecureRandom();

    List<Boolean> booleans = new ArrayList<>();

    /*******************************************
     * Name: CAGScene()
     * Description: Scene construtor
     * Parameters: CAGameManager
     * Returns: none
     *****************************************
     * @param pManager*/
    public Game(AGGameManager pManager) {
        super(pManager);
        AGSoundManager.vrMusic.loadMusic("musica.mp3", true);
        AGSoundManager.vrMusic.play();
    }

    @Override
    public void init() {

        boxum = null;
        boxdois = null;
        boxtres = null;

        gameover = null;
        restart = null;
        exit = null;

        perdeu = false;
        ganhou = false;
        gover = false;

        booleans.add(Boolean.FALSE);
        booleans.add(Boolean.FALSE);
        booleans.add(Boolean.TRUE);

        somBomb = AGSoundManager.vrSoundEffects.loadSoundEffect("bomb.mp3");

        somVitoria = AGSoundManager.vrSoundEffects.loadSoundEffect("vitoria.mp3");

        embaralhar();

        //Chamada toda vez que a cena for ativada, exibida
        setSceneBackgroundColor(0, 0, 0);

        reiniciarBoxs();

        criarPlacar(1, null);

        atualizarPlacar();
    }

    public void reiniciarBoxs() {
        embaralhar();
        atualizarPlacar();

        boxum = createSprite(R.mipmap.boxum, 1, 1);
        boxum.setScreenPercent(50, 25);
        boxum.vrPosition.setXY(AGScreenManager.iScreenWidth / 2, (AGScreenManager.iScreenHeight / 10) * 7.5f);

        boxdois = createSprite(R.mipmap.boxdois, 1, 1);
        boxdois.setScreenPercent(50, 25);
        boxdois.vrPosition.setXY(AGScreenManager.iScreenWidth / 2, (AGScreenManager.iScreenHeight / 10) * 4.5f);

        boxtres = createSprite(R.mipmap.boxtres, 1, 1);
        boxtres.setScreenPercent(50, 25);
        boxtres.vrPosition.setXY(AGScreenManager.iScreenWidth / 2, (AGScreenManager.iScreenHeight / 10) * 1.5f);
    }

    public void criarPlacar(int casas, AGVector2D agVector2D) {
        casas = casas < 10 ? 1 : casas < 100 ? 2 : casas < 1500 ? 3 : 4;
        vrPlacar = new AGSprite[casas];
        for (int index = 0; index < vrPlacar.length; index++) {
            vrPlacar[index] = createSprite(R.mipmap.fonte, 4, 4);
            if (agVector2D != null) {
                vrPlacar[index].setScreenPercent(30, 24);
            } else {
                vrPlacar[index].setScreenPercent(10, 8);
            }

            vrPlacar[index].bAutoRender = false;
            for (int pos = 0; pos < 10; pos++) {
                vrPlacar[index].addAnimation(1, false, pos);
            }

            if (index == 0) {
                if (agVector2D != null) {
                    vrPlacar[index].vrPosition.setXY(agVector2D.fX + 50, agVector2D.fY - 100);
                } else {
                    vrPlacar[index].vrPosition.setXY(AGScreenManager.iScreenWidth / 2, (AGScreenManager.iScreenHeight / 10) * 9.5f);
                }
            } else {
                vrPlacar[index].vrPosition.setXY(vrPlacar[index - 1].vrPosition.fX + vrPlacar[index].getSpriteWidth() / 2, (AGScreenManager.iScreenHeight / 10) * 9.5f);
            }
        }
    }

    @Override
    public void restart() {
        //Chamado após o retorno de uma interrupção
    }

    @Override
    public void stop() {
        //Chamado quando uma interrupção acontecer
    }

    @Override
    public void render() {
        super.render();
        desenhaPlacar();
    }

    @Override
    public void loop() {
        if (gover) {
            if (restart.collide(AGInputManager.vrTouchEvents.getLastPosition()) && AGInputManager.vrTouchEvents.screenClicked()) {
                vrGameManager.setCurrentScene(1);
                vrGameManager.setCurrentScene(2);

                score = 0;
                return;
            }
            if (exit.collide(AGInputManager.vrTouchEvents.getLastPosition()) && AGInputManager.vrTouchEvents.screenClicked()) {
                vrGameManager.setCurrentScene(1);
            }
        } else if (ganhou && !tempo.isTimeEnded()) {
            tempo.update();
        } else if (tempo.isTimeEnded() && ganhou) {
            reiniciarBoxs();
            tempo = new AGTimer(1500);
            ganhou = false;
        } else {

            if (perdeu && !gover && !tempo.isTimeEnded()) {
                tempo.update();
            } else if (tempo.isTimeEnded() && perdeu && !gover) {
                reiniciarBoxs();
                tempo = new AGTimer(1500);
                showGameOver();
            }
            if (!perdeu && boxum.collide(AGInputManager.vrTouchEvents.getLastPosition()) && AGInputManager.vrTouchEvents.screenClicked()) {
                if (booleans.get(0)) {
                    boxum = createSprite(R.mipmap.boxumstar, 1, 1);
                    score++;
                    atualizarPlacar();
                    ganhou();
                } else {
                    boxum = createSprite(R.mipmap.boxbomb, 1, 1);
                    perdeu();
                }
                boxum.setScreenPercent(50, 25);
                boxum.vrPosition.setXY(AGScreenManager.iScreenWidth / 2, (AGScreenManager.iScreenHeight / 10) * 7.5f);
            }

            if (!perdeu && boxdois.collide(AGInputManager.vrTouchEvents.getLastPosition()) && AGInputManager.vrTouchEvents.screenClicked()) {
                if (booleans.get(1)) {
                    boxdois = createSprite(R.mipmap.boxdoisstar, 1, 1);
                    score++;
                    atualizarPlacar();
                    ganhou();
                } else {
                    boxdois = createSprite(R.mipmap.boxbomb, 1, 1);
                    perdeu();
                }

                boxdois.setScreenPercent(50, 25);
                boxdois.vrPosition.setXY(AGScreenManager.iScreenWidth / 2, (AGScreenManager.iScreenHeight / 10) * 4.5f);
            }

            if (!perdeu && boxtres.collide(AGInputManager.vrTouchEvents.getLastPosition()) && AGInputManager.vrTouchEvents.screenClicked()) {
                if (booleans.get(2)) {
                    boxtres = createSprite(R.mipmap.boxtresstar, 1, 1);
                    score++;
                    atualizarPlacar();
                    ganhou();
                } else {
                    boxtres = createSprite(R.mipmap.boxbomb, 1, 1);
                    perdeu();
                }

                boxtres.setScreenPercent(50, 25);
                boxtres.vrPosition.setXY(AGScreenManager.iScreenWidth / 2, (AGScreenManager.iScreenHeight / 10) * 1.5f);
            }

        }
    }

    public void perdeu() {
        perdeu = true;
        AGSoundManager.vrSoundEffects.play(somBomb);
    }

    public void ganhou() {
        ganhou = true;
        AGSoundManager.vrSoundEffects.play(somVitoria);
    }

    public void embaralhar() {
        for (int i = 0; i < 10; i++) {
            Collections.shuffle(booleans);
        }
    }

    public void showGameOver() {
        gameover = createSprite(R.mipmap.gameover, 1, 1);
        gameover.setScreenPercent(60, 100);
        gameover.vrPosition.setXY(AGScreenManager.iScreenWidth / 2, AGScreenManager.iScreenHeight / 2);

        restart = createSprite(R.mipmap.restart, 1, 1);
        restart.setScreenPercent(13, 13);
        restart.vrPosition.setXY((AGScreenManager.iScreenWidth / 4), (AGScreenManager.iScreenHeight / 2) - 600);

        exit = createSprite(R.mipmap.exit, 1, 1);
        exit.setScreenPercent(13, 13);
        exit.vrPosition.setXY((AGScreenManager.iScreenWidth / 4) * 3, (AGScreenManager.iScreenHeight / 2) - 600);

        gover = true;
        atualizarPlacar();
        score = 0;
    }

    //Método para atualizar o placar: pegando cada número do score e adicionando no vetor para que possa ser desenhado posteriormente
    private void atualizarPlacar() {
        criarPlacar(score, gover? gameover.vrPosition : null);
        if (vrPlacar.length > 0)
            vrPlacar[0].setCurrentAnimation(score % 10);
        if (vrPlacar.length > 1)
            vrPlacar[1].setCurrentAnimation(score % 100 / 10);
        if (vrPlacar.length > 2)
            vrPlacar[2].setCurrentAnimation(score % 1500 / 100);
        if (vrPlacar.length > 3)
            vrPlacar[3].setCurrentAnimation(score % 15000 / 1500);
        if (vrPlacar.length > 4)
            vrPlacar[4].setCurrentAnimation(score % 150000 / 15000);
        if (vrPlacar.length > 5)
            vrPlacar[5].setCurrentAnimation(score / 150000);
    }

    //Método para desehar o placar na tela
    void desenhaPlacar() {
        //Loopong passando por cada número
        for (int index = 0; index < vrPlacar.length; index++) {
            //desenha os números 1 por 1
            vrPlacar[index].render();
        }
    }
}
