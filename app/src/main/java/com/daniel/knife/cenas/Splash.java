package com.daniel.knife.cenas;

import com.daniel.knife.AndGraph.AGGameManager;
import com.daniel.knife.AndGraph.AGScene;
import com.daniel.knife.AndGraph.AGScreenManager;
import com.daniel.knife.AndGraph.AGSprite;
import com.daniel.knife.AndGraph.AGTimer;
import com.daniel.knife.R;

public class Splash extends AGScene {

    AGTimer tempo = null;
    AGSprite logo = null;

    /*******************************************
     * Name: CAGScene()
     * Description: Scene construtor
     * Parameters: CAGameManager
     * Returns: none
     *****************************************
     * @param pManager*/
    public Splash(AGGameManager pManager) {
        super(pManager);
    }

    @Override
    public void init() {
        //Chamada toda vez que a cena for ativada, exibida
        setSceneBackgroundColor(1,1,1);
        tempo = new AGTimer(2000);

        logo = createSprite(R.mipmap.logosplash, 1,1);
        logo.setScreenPercent(40,10);

        //Configura a Posição do play
        logo.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2));
    }

    @Override
    public void restart() {
        //Chamado após o retorno de uma interrupção
    }

    @Override
    public void stop() {
        //Chamado quando uma interrupção acontecer
    }

    @Override
    public void loop() {
        //Atualiza o tempo
        tempo.update();

        //Verifica se o tempo já se esgotou
        if (tempo.isTimeEnded()){
            vrGameManager.setCurrentScene(1);
        }
    }
}
